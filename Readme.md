# Run Django on AWS Cloud 9(Linux AMI) with docker-compose

## Set-up

### Configure docker

### Setup docker-compose

### Set-up security groups

### Modify "ALLOWED_HOSTS" setting

### Start and run your django project


### Setup Postgresql (Optional)

* Inside **<projectName/settings.py>** , replace <span style="color:green;">DATABASES</span> with,
.....
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'postgres',
        'USER': 'postgres',
        'HOST': 'db',
        'PORT': 5432,
    }
}
.....