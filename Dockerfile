FROM python:latest
ENV PYTHONUNBUFFERED 1
RUN mkdir src
WORKDIR /src
ADD . /src/
RUN pip install -r requirements.txt
